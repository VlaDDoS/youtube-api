<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Video;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    private static int $numberOfComments = 1;

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Video::take(static::$numberOfComments)
            ->get()
            ->flatMap(fn(Video $video) => $this->forVideo($video))
            ->flatMap(fn (Comment $comment) => $this->repliesOf($comment))
            ->flatMap(fn (Comment $comment) => $this->repliesOf($comment))
            ->flatMap(fn (Comment $comment) => $this->repliesOf($comment));
    }

    private function forVideo(Video $video)
    {
        return Comment::factory(static::$numberOfComments)->for($video)->create();
    }

    private function repliesOf(Comment $comment) {
        return Comment::factory(static::$numberOfComments)->for($comment, 'parent')->create();
    }
}
